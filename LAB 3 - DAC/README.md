# DSP Architectures - LAB 3 - DAC

This is repository for university laboratories about Digital Signal Processors Architectures.

**This report was generated using README file from repository that holds my code:**

[Link to GitLab repository](https://gitlab.com/advanced-applied-electronics/semester-2/dsp-arch-lab/-/tree/master/LAB%203%20-%20DAC "https://gitlab.com/advanced-applied-electronics/semester-2/dsp-arch-lab/-/tree/master/LAB%203%20-%20DAC")

**Questions are answered at the end of this readme!!.**

**Table of contents:**

[[_TOC_]]

## Used software

- [STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html "https://www.st.com/en/development-tools/stm32cubeide.html")

- [Digilent Waveforms](https://digilent.com/shop/software/digilent-waveforms/ "https://digilent.com/shop/software/digilent-waveforms/")

- [Filter design websiteAdres URL](http://www.piclist.com/techref/uk/ac/york/cs/www-users/http/~fisher/mkfilter/trad.html "http://www.piclist.com/techref/uk/ac/york/cs/www-users/http/~fisher/mkfilter/trad.html")

- [Triangle WaveLook Up Table Generator Calculator](https://daycounter.com/Calculators/Triangle-Wave-Generator-Calculator2.phtml "https://daycounter.com/Calculators/Triangle-Wave-Generator-Calculator2.phtml")

## HW used during class

- [Analog Discovery 2](https://digilent.com/shop/analog-discovery-2-100ms-s-usb-oscilloscope-logic-analyzer-and-variable-power-supply/ "https://digilent.com/shop/analog-discovery-2-100ms-s-usb-oscilloscope-logic-analyzer-and-variable-power-supply/")

- [STM32F4DISCOVERY (STM32F407VG)](https://www.st.com/en/evaluation-tools/stm32f4discovery.html "https://www.st.com/en/evaluation-tools/stm32f4discovery.html")

- Second Order Low Pass RC Filter

![Second Order Low Pass RC Filter]( "Second Order Low Pass RC Filter")

## Task 1: ADC -> DAC

### No oversampling

During class I conducted measurements without oversampling. Here are the results:

#### 1kHz no oversampling

![1kHz no oversampling](./DSP_LAB3_T1/Measurements/1kHz.png "1kHz no oversampling")

#### 10kHz no oversampling

![10kHz no oversampling](./DSP_LAB3_T1/Measurements/10kHz.png "1kHz no oversampling")

#### 40kHz no oversampling

![40kHz no oversampling](./DSP_LAB3_T1/Measurements/40kHz.png "1kHz no oversampling")

#### 100kHz no oversampling

![100kHz no oversampling](./DSP_LAB3_T1/Measurements/100Hz.png "100kHz no oversampling")

## Task 2: Sine function generator

### During class

Curve achieved during class had wrong DAC trigger clock (2 times bigger). It was discovered using FFT on data from CSV file.

![Wrong frequency](./DPS_LAB3_T2/Results/Visualized%20data%20from%20from%20CSV%2C%20fs_8e%2B04.png)

![Wrong frequency FFT](./DPS_LAB3_T2/Results/FFT%20result%2C%20fs_8e%2B04.png)

On FFT plot we can see peak around 200 Hz while it should be around 100kHz / 1024 = 100Hz.

### At home

Correction was applied and the result were matching expectactions. Green is DAC clocking and yellow is the output curve.

![DAC Clocking](./DPS_LAB3_T2/Measurements/Sampling_Rate.png "DAC Clocking")

![Signal parameters](./DPS_LAB3_T2/Measurements/Signal%20parameters.png "Signal parameters")

![FFT](./DPS_LAB3_T2/Measurements/FFT.png "FFT")

I was generating sine consisting of 1024 points with frequency 100kHz, so 95.4Hz from FFT does match the theory.

### Frequency limit

Max generated frequency depends on sampling frequency of DAC. In order to meet sampling theorem we need at least 2 samples per period of generated signal. Unfortunately such signal will be useless for any practical application.

We ware using 1024 element wide LUT for generation, so max achievable frequency is DAC trigering frequency divided by 1024.

## Task 3: DDS

### Prepared DDS generator

Knowing that DDS generator will be useful in future task I prepared more universal code that is also possible to generate many instances on DDS generator with 2 possible shapes:

- sine wave (1024 LUT),
- triangle waveform (1024 LUT).

It is also possible to introdce phase shift to ther generated waveforms.

![DDS features](./DSP_LAB3_T3/Measurements/DDS_Sine_DAC_Sine_Triangle2.png "DDS features")

![Possibility to apply phase shift](./DSP_LAB3_T3/Measurements/DDS_Sine_DAC_Sine_Triangle_PhaseShift.png "Possibility to apply phase shift")

### DDS - 1kHz sine

![DDS - 1kHz sine](./DSP_LAB3_T3/Measurements/DDS_SINE_1kHz.png "DDS - 1kHz sine")

![DDS - 1kHz sine FFT](./DSP_LAB3_T3/Measurements/DDS_SINE_1kHz_FFT.png "DDS - 1kHz sine FFT")

### DDS - 1.37kHz sine

![DDS - 1.37kHz sine](./DSP_LAB3_T3/Measurements/DDS_SINE_1.37kHz.png "DDS - 1.37kHz sine")

![DDS - 1.37kHz sine FFT](./DSP_LAB3_T3/Measurements/DDS_SINE_1kHz_FFT.png "DDS - 1.37kHz sine FFT")

### DDS - 10kHz sine

![DDS - 10kHz sine](./DSP_LAB3_T3/Measurements/DDS_SINE_10kHz.png "DDS - 10kHz sine")

![DDS - 10kHz sine FFT](./DSP_LAB3_T3/Measurements/DDS_SINE_10kHz_FFT.png "DDS - 10kHz sine FFT")

### DDS - 30kHz sine

![DDS - 30kHz sine](./DSP_LAB3_T3/Measurements/DDS_SINE_30kHz.png "DDS - 30kHz sine")

![DDS - 30kHz sine FFT](./DSP_LAB3_T3/Measurements/DDS_SINE_30kHz_FFT.png "DDS - 30kHz sine FFT")

#### Used filter

![2nd order RC lowpass filter](./DSP_LAB3_T3/Measurements/2nd%20order%20RC%20lowpass.png "2nd order RC lowpass filter")

Theoretical transfer function

![Theoretical transfer function](./DSP_LAB3_T3/Measurements/filter_transfer_function.png "Theoretical transfer function")

### DDS - 30kHz sine after filtering

![DDS - 30kHz sine + filter](./DSP_LAB3_T3/Measurements/DDS_SINE_30kHz_FILTER.png "DDS - 30kHz sine + filter")

- Yellow - before filtering

- blue after filtering

![DDS - 30kHz sine + filter, FFT](./DSP_LAB3_T3/Measurements/DDS_SINE_30kHz_FFT_FILTER.png "DDS - 30kHz sine + filter, FFT")

Applied filter caused attenuation of all frequencies above 15kHz according to its transfer function plot. 30 kHz wave was attenuated by approximately 12dB. Higher frequencies were attenuated more so as a result 30kHz wave is the main frequency visible in filtered signal spectra.

### DDS - sin(x)/x corrector

A sinx/x corrector is a digital (or analog) filter used to compensate for the sinx/x roll-off (basically we have some attenuation for increasing frequency) inherent in the digital to analog conversion process.  

In DSP math, we treat the digital signal applied to the DAC is a sequence of impulses.  These are converted by the DAC into contiguous pulses of length Ts, where Ts is the DAC sample time. It is so callled zero order hold. The result is that the frequency response of the DAC is not flat; it is rather attenuated by sinc function. Attenuation depend on frequencies (sampling and DDS output):

![SINC ATTENUATION](./DSP_LAB3_T3/Measurements/SINC%20rolloff%20attenuation.png "SINC ATTENUATION")

We can compensate for the sinx/x roll-off by multiplying the amplitude of out DDS result by 1/H(f) calculated for given frequency.

Sinc (sin(x)/x) filter should be used along with antyaliasing lowpass filter when we have so callled zero order hold and we want to generate propper signall without rolloff attenuation.  

Bellow are results of my measurements.

Yellow wave is uncorrected chanel of DDS.

Gren wave is sinc corrected chanel of DDS.

In first case blue waveform is output from RC lowpass filter without SINC correction.

![DDS - 30kHz SINC, FILTERED NO CORRECTION](./DSP_LAB3_T3/Measurements/DDS_SINE_30kHz_NO_CORRECTION.png "DDS - 30kHz SINC, FILTERED NO CORRECTION")

Blue waveform one the second plot is output from RC lowpass filter with SINC correction.

![DDS - 30kHz SINC, FILTERED WITH CORRECTION](./DSP_LAB3_T3/Measurements/DDS_SINE_30kHz_WITH_CORRECTION.png "DDS - 30kHz SINC, FILTERED WITH CORRECTION")

Both with filter and without we can see that we have lowe amplitude when we dont use SINC correction @ 30kHz.

## Questions

### Question 1

**What is the advantages of DDS technique? Is it possible to perform DDS using only DMA with DAC in STM32F4?**

**Advantages**

- it uses precalculated LUT (look up table) so is faster than calculating every sample on the go,

- only limitations to this method are memory for LUT and max trigger rate of DAC,

- it is possible to achieve many waveforms.

**DDS using only DMA with DAC in STM32F4**

In my opinion it will be tricky. In order to get DDS working with known output frequency we need some kind of triggering. The best way to generate trigger with given frequency is to use timer. We can also use systick or other "software timer", but timer peripheral is better option. DMA does have many amazing features, interrupts and mechanisms, but non of them will provide flexible triggering with constant period.

## Author

[**Piotr Zieliński**](https://gitlab.com/piter_t_ziel)
