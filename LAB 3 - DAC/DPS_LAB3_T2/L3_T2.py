""" Plot data from .CSV file and run FFT """
from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
from tools import FileDirHelper
from tools import matplotlib_config
from scipy.fft import fft, fftfreq
from scipy.signal.windows import blackman, flattop
from engineering_notation import EngNumber


def main() -> None:
    """ Script entry point """
    
    matplotlib_config()
    show_plots = True

    png_helper = FileDirHelper(dir_name="Results", dbg_print=False)
    csv_helper = FileDirHelper(dir_name="Measurements", dbg_print=False, f_ext='.csv')

    file = 'T2_fast3'

    # ---------------| Read CSV |------------------ #
    # Find beginning of data
    data_start_line: int = None
    
    with open(csv_helper.file_path(file), 'r', encoding='UTF-8') as fp:
        for l_no, line in enumerate(fp):
            # search string
            if 'Time (s),Channel 1 (V),Channel 2 (V)' in line:
                print('string found in a file')
                print('Line Number:', l_no)
                print('Line:', line)
                data_start_line = l_no
                # don't look for next lines
                break

    df = pd.read_csv(csv_helper.file_path(file), skiprows=[i for i in range(data_start_line)])
    df.columns = ['Time (s)', 'Channel 1 (V)', 'Channel 2 (V)']
    
    time = df['Time (s)'].to_numpy()
    voltage = df['Channel 1 (V)'].to_numpy()

    ## Remove offset causing issues with windows used later
    #samples = samples - samples.mean()

    # Sample spacing
    Ts = time[1] - time[0]
    sampling_freq = 1/Ts
    

    # Number of sample points
    N = df['Time (s)'].size

    # Prepare windows for spectra leak correction
    #win_bm = blackman(N)
    #win_ft = flattop(N)

    # Plot the data set loaded from CSV.
    fig_id=1
    fig = plt.figure(fig_id)
    fig_name = f"Visualized data from from CSV, fs_{sampling_freq:.0}"
    #marker_size = 30
    fig.suptitle(fig_name)
    #plt.scatter(, samples,
    #            s=marker_size, c='firebrick')
    plt.plot(time, voltage,
                c='firebrick', label='original data')
    #plt.plot(sample_ids*Ts, samples*win_bm,
    #            c='blue', label='blackman windowing')
    #plt.plot(sample_ids*Ts, samples*win_ft,
    #            c='green', label='flat top windowing')
    #plt.ylim((0, (2**12) - 1))
    plt.xlabel('Time (s)')
    plt.ylabel('Channel 1 (V)')
    #plt.legend()
    plt.tight_layout()
    fig.savefig(fname=png_helper.file_path(fig_name), bbox_inches='tight')

    if not show_plots:
        plt.close(fig)
  
    #-----------------| FFT |-------------------
    xf = fftfreq(N, Ts)[1:N//2]

    yf = 2*np.abs(fft(voltage)[1:N//2])/N
    
    # Windowing
    #ywf = 2*np.abs(fft(samples*win_bm)[1:N//2])/N
    #yftf = 2*np.abs(fft(samples*win_ft)[1:N//2])/N

    # Plot the fft result.
    fig_id+=1
    fig = plt.figure(fig_id)
    fig_name = f"FFT result, fs_{sampling_freq:.0}"
    marker_size = 30
    fig.suptitle(fig_name)
    plt.plot(xf, yf, label='FFT uniform')
    #plt.scatter(xf, yf)
    #plt.plot(xf, ywf, label='FFT blackman window')
    #plt.scatter(xf, ywf)
    #plt.plot(xf, yftf, label='FFT flat top window')
    #plt.scatter(xf, yftf)
    plt.xlabel('frequency [Hz]')
    plt.xscale('log')
    plt.ylabel('Channel 1 (V)')
    #plt.legend()
    plt.tight_layout()
    fig.savefig(fname=png_helper.file_path(fig_name), bbox_inches='tight')

    if not show_plots:
        plt.close(fig)


    # Show all plots
    if show_plots:
        plt.show(block=False)
        input("Press enter to close all plots >>>>")
        plt.close("all")

# Module entry point
if __name__ == '__main__':
    main()
