# DSP Architectures - LAB 4 - Filters & Modulation

This is repository for university laboratories about Digital Signal Processors Architectures.

**This report was generated using README file from repository that holds my code:**

[Link to GitLab repository](https://gitlab.com/advanced-applied-electronics/semester-2/dsp-arch-lab/-/tree/master/LAB%204%20-%20Filters%20%26%20modulation "https://gitlab.com/advanced-applied-electronics/semester-2/dsp-arch-lab/-/tree/master/LAB%204%20-%20Filters%20%26%20modulation")

**Questions are answered at the end of this readme!!.**

**Table of contents:**

[[_TOC_]]

## Used software

- [STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html "https://www.st.com/en/development-tools/stm32cubeide.html")

- [Digilent Waveforms](https://digilent.com/shop/software/digilent-waveforms/ "https://digilent.com/shop/software/digilent-waveforms/")

- [Filter design websiteAdres URL](http://www.piclist.com/techref/uk/ac/york/cs/www-users/http/~fisher/mkfilter/trad.html "http://www.piclist.com/techref/uk/ac/york/cs/www-users/http/~fisher/mkfilter/trad.html")

- [Sine wave Look Up Table Generator Calculator](https://daycounter.com/Calculators/Sine-Generator-Calculator.phtml "https://daycounter.com/Calculators/Sine-Generator-Calculator.phtml")

- [Triangle wave Look Up Table Generator Calculator](https://daycounter.com/Calculators/Triangle-Wave-Generator-Calculator.phtml "https://daycounter.com/Calculators/Triangle-Wave-Generator-Calculator.phtml")

## HW used during class

- [Analog Discovery 2](https://digilent.com/shop/analog-discovery-2-100ms-s-usb-oscilloscope-logic-analyzer-and-variable-power-supply/ "https://digilent.com/shop/analog-discovery-2-100ms-s-usb-oscilloscope-logic-analyzer-and-variable-power-supply/")

- [STM32F4DISCOVERY (STM32F407VG)](https://www.st.com/en/evaluation-tools/stm32f4discovery.html "https://www.st.com/en/evaluation-tools/stm32f4discovery.html")

## Task 1 - AM Modulation

![AM modulation](./DSP_LAB4_T1/Measurements/AM_detailed.png "AM modulation")

![AM modulation](./DSP_LAB4_T1/Measurements/AM_many_periods.png "AM modulation")

![AM modulation - FFT](./DSP_LAB4_T1/Measurements/AM_FFT.png "AM modulation - FFT")

![AM modulation - Spectrogram 2D](./DSP_LAB4_T1/Measurements/AM_Spectrogram.png)

![AM modulation - Spectrogram 3D](./DSP_LAB4_T1/Measurements/AM_3D_Spectrogram.png)

## Task 2 - FM Modulation

![FM modulation](./DSP_LAB4_T2/Measurements/FM_Detailed.png "FM modulation")

![FM modulation](./DSP_LAB4_T2/Measurements/FM_Many_Periods.png "FM modulation")

![FM modulation - FFT](./DSP_LAB4_T2/Measurements/FM_FFT.png "FM modulation - FFT")

![FM modulation - Spectrogram 2D](./DSP_LAB4_T2/Measurements/FM_Spectrogram.png)

![FM modulation - Spectrogram 3D](./DSP_LAB4_T2/Measurements/FM_3D_Spectrogram.png)

## Task 3 - IIR Filters

Coefficients for filters were generated using this online [utility](http://www.piclist.com/techref/uk/ac/york/cs/www-users/http/~fisher/mkfilter/trad.html "http://www.piclist.com/techref/uk/ac/york/cs/www-users/http/~fisher/mkfilter/trad.html").

**Low pass filter @ 500Hz:**

![Low pass filter @ 500Hz](./DSP_LAB4_T3/DOC/LOW%20PASS%20%40%20500HZ.png)

**Bode plot for low pass filter**

![Bode Plot of Low Pass Filter](./DSP_LAB4_T3/DOC/LOWPASS.png)

**Bode plot for Band Pass filter**

![Bode plot for Band Pass filter](./DSP_LAB4_T3/DOC/BANDPASS.png)

**Both filters/DAC channels measured at once**

![Both filters/DAC channels measured at once](./DSP_LAB4_T3/DOC/BOTH%20FILTERS.png)

## Questions

### What is the spectrum of AM and FM modulation?

**AM modulation**

![AM modulation spectrum](./DOC/AM.png)

**FM modulation**

![FM modulation spectrum](./DOC/FM.png)

### What are advantages and disadvantages of FIR and IIR filters?

**Advantages of  FIR filters :**

- FIR filter are always stable

- FIR filters can achieve linear phase response and pass a signal without phase distortion.

- It is easy to optimize and implement than IIR filters.

- Both recursive, as well as nonrecursive filter, can be designed using FIR designing techniques

**Disadvantages of FIR filters:**

- Large storage requirements

- Can not simulate prototype analog filter

- Expensive for larger orders

**Advantages of IIR filters:**

- The advantage of IIR filters over FIR filters is that IIR filters usually require fewer coefficients to execute similar filtering operations, that IIR filters work faster, and require less memory space.

**Disadvantages of IIR filter:**

- The disadvantage of IIR filters is the nonlinear phase response. IIR filters are well suited for applications that require no phase information, for example, for monitoring the signal amplitudes.

- They are harder to implement using fixed-point arithmetic.

## Author

[**Piotr Zieliński**](https://gitlab.com/piter_t_ziel)
