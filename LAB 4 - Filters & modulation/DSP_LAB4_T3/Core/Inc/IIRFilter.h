/*
 * IIRFilter.h
 *
 *  Created on: Jan 17, 2023
 *      Author: Piotr
 */

#ifndef INC_IIRFILTER_H_
#define INC_IIRFILTER_H_


typedef struct IIR_3rd_ord_butterworth_LP_t
{
	float xv[4]; 	// Input register
	float yv[4];	// Output register
	float a[4];		// Input register coefficients
	float b[4];		// Output register coefficients
	float gain;
}IIR_3rd_ord_butterworth_LP_t;

typedef struct IIR_2nd_ord_butterworth_BP_t
{
	float xv[5]; 	// Input register
	float yv[5];	// Output register
	float a[5];		// Input register coefficients
	float b[5];		// Output register coefficients
	float gain;
}IIR_2nd_ord_butterworth_BP_t;

void IIR_3rd_ord_butterworth_LP_Init(IIR_3rd_ord_butterworth_LP_t* prtFILTER);
float IIR_3rd_ord_butterworth_LP_Task(IIR_3rd_ord_butterworth_LP_t* prtFILTER, float new_input);

void IIR_2nd_ord_butterworth_BP_Init(IIR_2nd_ord_butterworth_BP_t* prtFILTER);
float IIR_2nd_ord_butterworth_BP_Task(IIR_2nd_ord_butterworth_BP_t* prtFILTER, float new_input);


#endif /* INC_IIRFILTER_H_ */
