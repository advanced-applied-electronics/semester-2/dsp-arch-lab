/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dac.h"
#include "dma.h"
#include "tim.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "IIRFilter.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

volatile uint16_t adc_value;
volatile uint16_t dac_ch1_value;
volatile uint16_t dac_ch2_value;

IIR_3rd_ord_butterworth_LP_t lpFilter;
IIR_2nd_ord_butterworth_BP_t bpFilter;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

void StartFilter();
void TIM_SetFrequency(TIM_HandleTypeDef* htim_ptr, uint32_t new_freq);
uint32_t TIM_GetFrequency(TIM_HandleTypeDef* htim_ptr);
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc);

// Prepare ADC offset
//const uint16_t ADC_Offset = ADC_MAX_VAL/2;
const uint16_t ADC_Offset = ADC_MAX_VAL * 1.5f / 3.3f;

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_DAC_Init();
  MX_TIM2_Init();
  MX_ADC1_Init();
  /* USER CODE BEGIN 2 */

  IIR_3rd_ord_butterworth_LP_Init(&lpFilter);
  IIR_2nd_ord_butterworth_BP_Init(&bpFilter);
  StartFilter();


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	volatile float measurement = adc_value;


	// Remove offset
	measurement = measurement - ADC_Offset;

	dac_ch1_value = (uint16_t)(IIR_3rd_ord_butterworth_LP_Task(&lpFilter, measurement) + ADC_Offset);
	dac_ch2_value = (uint16_t)(IIR_2nd_ord_butterworth_BP_Task(&bpFilter, measurement) + ADC_Offset);
}


void StartFilter()
{
	// Timer setup for 100kHz triggering
	TIM_SetFrequency(&htim2, (uint32_t)(100E3));
	volatile uint32_t freq = TIM_GetFrequency(&htim2);

	HAL_ADC_Start_DMA(&hadc1, (uint32_t*)(&adc_value), 1);


	HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1,
			(uint32_t*)(&dac_ch1_value), 1, DAC_ALIGN_12B_R);
	HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_2,
			(uint32_t*)(&dac_ch2_value), 1, DAC_ALIGN_12B_R);

	// __HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1,  100);
	// HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
	HAL_TIM_Base_Start(&htim2);
}


void TIM_SetFrequency(TIM_HandleTypeDef* htim_ptr, uint32_t new_freq)
{
	/* Check if correct timer is handled in this function */
	if (htim_ptr->Instance != htim2.Instance)
		while (1);

	/* Extract info about APB divider */
	RCC_ClkInitTypeDef RCC_config;
	uint32_t FLatency;
	HAL_RCC_GetClockConfig(&RCC_config, &FLatency);

	/* Get timer_clocking_freq basing on APB divider */
	uint32_t timer_clocking_freq;

	if (RCC_config.APB1CLKDivider == RCC_HCLK_DIV1)
		timer_clocking_freq = HAL_RCC_GetPCLK1Freq();
	else
		timer_clocking_freq = 2 * HAL_RCC_GetPCLK1Freq();

	/* Check if PSC is necessary */
	float estimate_ARR = (float)(timer_clocking_freq) / new_freq - 1;

	if (estimate_ARR >= TIM2_ARR_MAX)
		while (1) {}

	/* Calculate ARR */
	uint32_t ARR = timer_clocking_freq / new_freq - 1;

	/* Set ARR */
	__HAL_TIM_SET_AUTORELOAD(htim_ptr, ARR);

	/* Check in debug mode if everything is ok */
	//volatile uint32_t freq = TIM_GetFrequency(&htim2);
}


uint32_t TIM_GetFrequency(TIM_HandleTypeDef* htim_ptr)
{
	/* Check if correct timer is handled in this function */
	if (htim_ptr->Instance != htim2.Instance)
		while (1);

	/* Extract info about APB divider */
	RCC_ClkInitTypeDef RCC_config;
	uint32_t FLatency;
	HAL_RCC_GetClockConfig(&RCC_config, &FLatency);

	/* Get timer_clocking_freq basing on APB divider */
	uint32_t timer_clocking_freq;

	if (RCC_config.APB1CLKDivider == RCC_HCLK_DIV1)
		timer_clocking_freq = HAL_RCC_GetPCLK1Freq();
	else
		timer_clocking_freq = 2 * HAL_RCC_GetPCLK1Freq();

	/* Calculate frequency */
	uint32_t timer_freq = timer_clocking_freq;
	timer_freq /= ((htim_ptr->Instance->ARR + 1)*(htim_ptr->Instance->PSC + 1));

	return timer_freq;
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
