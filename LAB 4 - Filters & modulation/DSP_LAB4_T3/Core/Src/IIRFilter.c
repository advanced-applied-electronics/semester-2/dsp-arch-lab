/*
 * IIRFilter.c
 *
 *  Created on: Jan 17, 2023
 *      Author: Piotr
 */

#include "IIRFilter.h"


/*
 * IIRFilter.h
 *
 *  Created on: Jan 17, 2023
 *      Author: Piotr
 */

void IIR_3rd_ord_butterworth_LP_Init(IIR_3rd_ord_butterworth_LP_t* prtFILTER)
{
	static const float input_coefs[4] = {1.0, 3, 3, 1};
	static const float output_coefs[4] = { 8.8189313059e-001,
			-2.7564831952e+000,
			 2.8743568927e+000,
			-1.0000000000e+000};
	static const float gain_new = 3.430944333e+004;

	memcpy(prtFILTER->a, input_coefs, sizeof(prtFILTER->a));
	memcpy(prtFILTER->b, output_coefs, sizeof(prtFILTER->b));
	prtFILTER->gain = gain_new;
}


float IIR_3rd_ord_butterworth_LP_Task(IIR_3rd_ord_butterworth_LP_t* prtFILTER, float new_input)
 {
	// Rewrite last input samples
	prtFILTER->xv[0] = prtFILTER->xv[1];
	prtFILTER->xv[1] = prtFILTER->xv[2];
	prtFILTER->xv[2] = prtFILTER->xv[3];

	// Calculate new input
	prtFILTER->xv[3] = new_input / prtFILTER->gain;

	// Rewrite last output samples
	prtFILTER->yv[0] = prtFILTER->yv[1];
	prtFILTER->yv[1] = prtFILTER->yv[2];
	prtFILTER->yv[2] = prtFILTER->yv[3];

	/* -----| Calculate new output |----*/
	// Impact of input samples
	prtFILTER->yv[3]  = prtFILTER->a[0] * prtFILTER->xv[0];
	prtFILTER->yv[3] += prtFILTER->a[1] * prtFILTER->xv[1];
	prtFILTER->yv[3] += prtFILTER->a[2] * prtFILTER->xv[2];
	prtFILTER->yv[3] += prtFILTER->a[3] * prtFILTER->xv[3];

	// Impact of output samples
	prtFILTER->yv[3] += prtFILTER->b[0] * prtFILTER->yv[0];
	prtFILTER->yv[3] += prtFILTER->b[1] * prtFILTER->yv[1];
	prtFILTER->yv[3] += prtFILTER->b[2] * prtFILTER->yv[2];

	return prtFILTER->yv[3]; // Next output
}


void IIR_2nd_ord_butterworth_BP_Init(IIR_2nd_ord_butterworth_BP_t* prtFILTER)
{
	static const float input_coefs[5] = {1.0f, 0.0f, -2.0f, 0.0f, 1.0f};
	static const float output_coefs[5] = {-8.7521454825e-001,
			3.6101781308e+000,
			-5.5942343734e+000,
			3.8592561983e+000,
			-1.0000000000e+000};

	static const float gain_new = 4.7871770420e+002;

	memcpy(prtFILTER->a, input_coefs, sizeof(prtFILTER->a));
	memcpy(prtFILTER->b, output_coefs, sizeof(prtFILTER->b));
	prtFILTER->gain = gain_new;
}


float IIR_2nd_ord_butterworth_BP_Task(IIR_2nd_ord_butterworth_BP_t* prtFILTER, float new_input)
 {
	// Rewrite last input samples
	prtFILTER->xv[0] = prtFILTER->xv[1];
	prtFILTER->xv[1] = prtFILTER->xv[2];
	prtFILTER->xv[2] = prtFILTER->xv[3];
	prtFILTER->xv[3] = prtFILTER->xv[4];

	// Calculate new input
	prtFILTER->xv[4] = new_input / prtFILTER->gain;

	// Rewrite last output samples
	prtFILTER->yv[0] = prtFILTER->yv[1];
	prtFILTER->yv[1] = prtFILTER->yv[2];
	prtFILTER->yv[2] = prtFILTER->yv[3];
	prtFILTER->yv[3] = prtFILTER->yv[4];

	/* -----| Calculate new output |----*/
	// Impact of input samples
	prtFILTER->yv[4]  = prtFILTER->a[0] * prtFILTER->xv[0];
	prtFILTER->yv[4] += prtFILTER->a[1] * prtFILTER->xv[1];
	prtFILTER->yv[4] += prtFILTER->a[2] * prtFILTER->xv[2];
	prtFILTER->yv[4] += prtFILTER->a[3] * prtFILTER->xv[3];
	prtFILTER->yv[4] += prtFILTER->a[4] * prtFILTER->xv[4];

	// Impact of output samples
	prtFILTER->yv[4] += prtFILTER->b[0] * prtFILTER->yv[0];
	prtFILTER->yv[4] += prtFILTER->b[1] * prtFILTER->yv[1];
	prtFILTER->yv[4] += prtFILTER->b[2] * prtFILTER->yv[2];
	prtFILTER->yv[4] += prtFILTER->b[3] * prtFILTER->yv[3];

	return prtFILTER->yv[4]; // Next output
}
