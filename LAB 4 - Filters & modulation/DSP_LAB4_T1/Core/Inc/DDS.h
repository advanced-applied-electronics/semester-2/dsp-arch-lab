/*
 * DDS.h
 *
 *  Created on: Dec 12, 2022
 *      Author: piter
 */

#ifndef INC_DDS_H_
#define INC_DDS_H_

#include "stdint.h"

#define DDS_SINE_LUT_LEN 		(1024)
#define DDS_SINE_LUT_HALF_VAL 	(32768)


#define DDS_TRIANGLE_LUT_LEN		(1024)
#define DDS_TRIANGLE_LUT_HALF_VAL 	(32768)

typedef enum DDS_Shape_t
{
	DDS_Shape_Sine,
	DDS_Shape_Triangle
} DDS_Shape_t;


typedef struct DDS_Instance_t
{
	DDS_Shape_t shape;
	float amp;		// Amplitude within range (-1, 1)
	uint32_t outFreq;	// Output frequency
	uint32_t sampleFreq;	// Sampling frequency
	float initPhase;		// Initial phase value from range (0, DDS_SINE_LUT_LEN or DDS_TRIANGLE_LUT_LEN)
	float phaseAcumulator;
	float phaseStep;
}DDS_Instance_t;


void DDS_Init(DDS_Instance_t* instPtr);
uint16_t DDS_Calculate(DDS_Instance_t* instPtr);



#endif /* INC_DDS_H_ */
