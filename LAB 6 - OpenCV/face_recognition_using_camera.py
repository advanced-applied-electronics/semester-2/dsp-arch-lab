import cv2

def main() -> None:
    """ Script entry point """
      
    # Load the cascade
    face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades +  'haarcascade_frontalface_default.xml')

    # To capture video from webcam. 
    cap = cv2.VideoCapture(0)

    while True:
        # Read the frame
        _, img = cap.read()
        
        # Convert to grayscale
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        
        # Detect the faces
        faces = face_cascade.detectMultiScale(gray, 1.1, 4)
        
        fontSize = 0.8
        font = cv2.FONT_HERSHEY_COMPLEX
        
        # Draw the rectangle around each face and print coordinates
        for (x, y, w, h) in faces:
            cv2.rectangle(img, (x, y), (x+w, y+h), (0, 0, 250), 1)    
            text_l1 = f"X:{x}, Y:{y}"
            text_l2 = f"size: {w}x{h}"
            
            (_,offset),_ = cv2.getTextSize(text=text_l2, fontFace=font, fontScale=fontSize, thickness=2)
            cv2.putText(img, text_l1, (x,y-offset), font, fontSize, (0, 0, 250), 1, cv2.LINE_AA)
            cv2.putText(img, text_l2, (x,y), font, fontSize, (0, 0, 250), 1, cv2.LINE_AA)

        # Display
        cv2.imshow('img', img)
        
        # Stop if escape key is pressed
        k = cv2.waitKey(30) & 0xff
        if k==27:
            break
        
    # Release the VideoCapture object
    cap.release()
    
# Module entry point
if __name__ == '__main__':
    main()