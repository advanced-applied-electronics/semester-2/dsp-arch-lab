# DSP Architectures - LAB 1 - LED Blink

This is repository for university laboratories about Digital Signal Processors Architectures.

## Used software

- [STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html "https://www.st.com/en/development-tools/stm32cubeide.html")

- [Digilent Waveforms](https://digilent.com/shop/software/digilent-waveforms/ "https://digilent.com/shop/software/digilent-waveforms/")

## Used HW

- [Analog Discovery 2](https://digilent.com/shop/analog-discovery-2-100ms-s-usb-oscilloscope-logic-analyzer-and-variable-power-supply/ "https://digilent.com/shop/analog-discovery-2-100ms-s-usb-oscilloscope-logic-analyzer-and-variable-power-supply/")

- [STM32F4DISCOVERY (STM32F407VG)](https://www.st.com/en/evaluation-tools/stm32f4discovery.html "https://www.st.com/en/evaluation-tools/stm32f4discovery.html")

## Author

[**Piotr Zieliński**](https://gitlab.com/piter_t_ziel)
