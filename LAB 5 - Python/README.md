# DSP Architectures - LAB 5 - Python

This is repository for university laboratories about Digital Signal Processors Architectures.

**Please check out Jupyter notebook:**

[Lab 5 notebook](./HearthRate.ipynb)

## Author

[**Piotr Zieliński**](https://gitlab.com/piter_t_ziel)
