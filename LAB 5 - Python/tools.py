" Script with tools and not important code "
import os
import sys
import matplotlib.pyplot as plt
from matplotlib import rcParams

def matplotlib_config() -> None:
    "MatPlotLib figures configuration"

    plt.style.use("ggplot")
    rcParams["figure.figsize"] = [7, 7]
    rcParams["figure.dpi"] = 100
    rcParams["savefig.dpi"] = 150
    #rcParams["figure.autolayout"] = True # plt.tight_layout()

class FileDirHelper:
    """ Helper for creating directory and preparing
        strings in order to save files."""

    def __init__(self, dir_name: str, f_ext: str = ".png", dbg_print: bool = True) -> None:
        """ Constructor of FileDirHelper instance

        Args:
            dir_name (str): Name of directory that will be created
                in the location of main interpreted script.
            
            f_ext (str, optional): Extension of saved files. Defaults to ".png".

            dbg_print (bool, optional): Should debug string be printed? Defaults to True.
        """

        # Instance variables:
        self._script_directory_path: str = None
        self.figure_dir_path: str = None
        self.dbg_print: bool = dbg_print
        self.f_ext: str = f_ext

        # Get the directory path of main interpreted script
        self._script_directory_path = os.path.dirname(os.path.realpath(sys.argv[0]))

        # Prepare path to directory
        self.figure_dir_path = os.path.join(self._script_directory_path, dir_name)

        # Create figure_dir if don't exist
        if not os.path.exists(self.figure_dir_path):
            os.mkdir(self.figure_dir_path)

    def file_path(self, file_name: str) -> str:
        """ Prepare path for saving file

        Args:
            file_name (str): Name of file

        Returns:
            str: Path to new file
        """
        file_path = os.path.join(self.figure_dir_path,
                                 file_name + self.f_ext)

        if self.dbg_print:
            print(F"Created file \"{file_path}\".\n")

        return file_path
