""" Plot data from .CSV file """
from matplotlib import pyplot as plt
import pandas as pd
import numpy as np
from tools import FileDirHelper
from tools import matplotlib_config
from scipy.fft import fft, fftfreq
from scipy.signal.windows import blackman, flattop
from engineering_notation import EngNumber


def main() -> None:
    """ Script entry point """
    matplotlib_config()
    show_plots = True

    png_helper = FileDirHelper(dir_name="Results", dbg_print=False)
    csv_helper = FileDirHelper(dir_name="measurements", dbg_print=False, f_ext='.csv')

    measured_freq = "1kHz cfg 1"
    file = 'config1_1kHz'
    sampling_freq = 50E3

    # Read CSV
    df = pd.read_csv(csv_helper.file_path(file))
    df.columns = ['sample id', 'value']
    samples = df['value'].to_numpy()
    sample_ids = df['sample id'].to_numpy()

    ## Remove offset causing issues with windows used later
    #samples = samples - samples.mean()

    # Sample spacing
    Ts = 1.0 / sampling_freq

    # Number of sample points
    N = df['value'].size

    # Prepare windows for spectra leak correction
    #win_bm = blackman(N)
    #win_ft = flattop(N)

    # Plot the data set loaded from CSV.
    fig_id=1
    fig = plt.figure(fig_id)
    fig_name = f"Visualized data from from CSV, {measured_freq}, fs_{sampling_freq:.0}"
    marker_size = 30
    fig.suptitle(fig_name)
    plt.scatter(sample_ids*Ts, samples,
                s=marker_size, c='firebrick')
    plt.plot(sample_ids*Ts, samples,
                c='firebrick', label='original data')
    #plt.plot(sample_ids*Ts, samples*win_bm,
    #            c='blue', label='blackman windowing')
    #plt.plot(sample_ids*Ts, samples*win_ft,
    #            c='green', label='flat top windowing')
    #plt.ylim((0, (2**12) - 1))
    plt.xlabel('time [s]')
    plt.ylabel('ADC value')
    #plt.legend()
    plt.tight_layout()
    fig.savefig(fname=png_helper.file_path(fig_name), bbox_inches='tight')

    if not show_plots:
        plt.close(fig)
  
    #-----------------| FFT |-------------------
    xf = fftfreq(N, Ts)[1:N//2]

    yf = 2*np.abs(fft(samples)[1:N//2])/N

    # Windowing
    #ywf = 2*np.abs(fft(samples*win_bm)[1:N//2])/N
    #yftf = 2*np.abs(fft(samples*win_ft)[1:N//2])/N

    # Plot the fft result.
    fig_id+=1
    fig = plt.figure(fig_id)
    fig_name = f"FFT result, {measured_freq}, fs_{sampling_freq:.0}"
    marker_size = 30
    fig.suptitle(fig_name)
    plt.plot(xf, yf, label='FFT uniform')
    plt.scatter(xf, yf)
    #plt.plot(xf, ywf, label='FFT blackman window')
    #plt.scatter(xf, ywf)
    #plt.plot(xf, yftf, label='FFT flat top window')
    #plt.scatter(xf, yftf)
    plt.xlabel('frequency [Hz]')
    plt.xscale("log")
    #plt.legend()
    plt.tight_layout()
    fig.savefig(fname=png_helper.file_path(fig_name), bbox_inches='tight')
    
    #----------------| Calculate sinad and enob|-------------
    # https://www.edaboard.com/threads/few-clarification-on-snr-sndr-calculation.155488/
    # https://e2e.ti.com/support/data-converters-group/data-converters/f/data-converters-forum/607074/matlab-code-to-calculate-snr-sndr-thd-sfdr
    # https://www.researchgate.net/deref/http%3A%2F%2Fwww.analog.com%2Fstatic%2Fimported-files%2Ftutorials%2FMT-003.pdf
    signal_id = np.argmax(yf)
    signal_power = 20*np.log10(np.sum(yf[signal_id-30:signal_id+29]))
    print(f"signal_power = {signal_power}")

    sinad_power = 10*np.log10(np.sum(yf[3:-3])**2) - signal_power
    print(f"sinad_power = {sinad_power}")

    sinad = 20*np.log10(signal_power/sinad_power)
    print(f"sinad [dB] = {sinad}")
    
    enob = (20**((sinad - 1.76)/20)) / 6.02
    print(f"enob = {enob}")

    if not show_plots:
        plt.close(fig)

    # Show all plots
    if show_plots:
        plt.show(block=False)
        input("Press enter to close all plots >>>>")
        plt.close("all")

# Module entry point
if __name__ == '__main__':
    main()
