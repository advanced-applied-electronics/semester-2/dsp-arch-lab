# DSP Architectures - LAB 2 - ADC

This is repository for university laboratories about Digital Signal Processors Architectures.

**This report was generated using README file from repository that holds my code:**

https://gitlab.com/advanced-applied-electronics/semester-2/dsp-arch-lab/-/tree/master/LAB%202%20-%20ADC

**Questions are answered at the end of this readme!!.**

**Table of contents:**

[[_TOC_]]

## Used software

- [STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html "https://www.st.com/en/development-tools/stm32cubeide.html")

- [Digilent Waveforms](https://digilent.com/shop/software/digilent-waveforms/ "https://digilent.com/shop/software/digilent-waveforms/")

## HW used during class

- [Analog Discovery 2](https://digilent.com/shop/analog-discovery-2-100ms-s-usb-oscilloscope-logic-analyzer-and-variable-power-supply/ "https://digilent.com/shop/analog-discovery-2-100ms-s-usb-oscilloscope-logic-analyzer-and-variable-power-supply/")

- [STM32F4DISCOVERY (STM32F407VG)](https://www.st.com/en/evaluation-tools/stm32f4discovery.html "https://www.st.com/en/evaluation-tools/stm32f4discovery.html"),

- FT232 UART to USB converter (for transfering measured data to PC).

## HW used at home

- [STM32F4DISCOVERY (STM32F407VG)](https://www.st.com/en/evaluation-tools/stm32f4discovery.html "https://www.st.com/en/evaluation-tools/stm32f4discovery.html"),

- phone's audio as function generator (generation of 1kHz, 10 kHz and 20kHz sines) + noninverting level shifter (single supply) opamp circuit 
- FT232 UART to USB converter (for transfering measured data to PC).

![Measurement setup](./DOC/MEASUREMENT_SETUP.jpg "")

### Detailed data about created level shifter

All documentqation along with spice simulation can be found in this [directory](./Single%20supply%20noninverting%20level%20shifter/).

![Crude PCB with Opamp](./Single%20supply%20noninverting%20level%20shifter/CRUDE_PCB.jpg)

![Schematic](./Single%20supply%20noninverting%20level%20shifter/Schematic.png)

![Plots](./Single%20supply%20noninverting%20level%20shifter/Plots.png)

#### Measuremetns for input

![Input](./Single%20supply%20noninverting%20level%20shifter/IN.png)

#### Measuremetns for output

![Input](./Single%20supply%20noninverting%20level%20shifter/OUT.png)

## Measured data

Measured data can be found in [measuremets directory](./measurements/).

Example plots present measured sine waves and square wave is ADC trigger.

![50 kHz sampling](./DOC/50kHzSampling.png)

![800 kHz sampling](./DOC/800kHzSampling.png)

## Results

### Measurements recorder during class (without DMA)

1kHz

![Samples](./Results/Visualized%20data%20from%20from%20CSV%2C%201kHz%20measred%20in%20class%2C%20fs_5e%2B04.png)

![FFT](./Results/FFT%20result%2C%201kHz%20measred%20in%20class%2C%20fs_5e%2B04.png)

10kHz

![Samples](./Results/Visualized%20data%20from%20from%20CSV%2C%2010kHz%20measred%20in%20class%2C%20fs_5e%2B04.png)

![FFT](./Results/FFT%20result%2C%2010kHz%20measred%20in%20class%2C%20fs_5e%2B04.png)

40kHz

![Samples](./Results/Visualized%20data%20from%20from%20CSV%2C%2040kHz%20measred%20in%20class%2C%20fs_5e%2B04.png)

![FFT](./Results/FFT%20result%2C%2040kHz%20measred%20in%20class%2C%20fs_5e%2B04.png)

### Measuremetns recorded at home using my hardwares (no DMA and with DMA)

I was capturing 2048 samples every time. Plots  already present time in [s].

1kHz

![Samples](./Results/Visualized%20data%20from%20from%20CSV%2C%201kHz%20cfg%201%2C%20fs_5e%2B04.png)

![Samples](./Results/Visualized%20data%20from%20from%20CSV%2C%201kHz%20cfg%202%2C%20fs_5e%2B04.png)

![FFT](./Results/FFT%20result%2C%201kHz%20cfg%201%2C%20fs_5e%2B04.png)

![FFT](./Results/FFT%20result%2C%201kHz%20cfg%202%2C%20fs_5e%2B04.png)

10kHz

![Samples](./Results/Visualized%20data%20from%20from%20CSV%2C%2010kHz%20cfg%201%2C%20fs_5e%2B04.png)

![Samples](./Results/Visualized%20data%20from%20from%20CSV%2C%2010kHz%20cfg%202%2C%20fs_5e%2B04.png)

![FFT](./Results/FFT%20result%2C%2010kHz%20cfg%201%2C%20fs_5e%2B04.png)

![FFT](./Results/FFT%20result%2C%2010kHz%20cfg%202%2C%20fs_5e%2B04.png)

20kHz

![Samples](./Results/Visualized%20data%20from%20from%20CSV%2C%2020kHz%20cfg%201%2C%20fs_5e%2B04.png)

![Samples](./Results/Visualized%20data%20from%20from%20CSV%2C%2020kHz%20cfg%202%2C%20fs_5e%2B04.png)

![FFT](./Results/FFT%20result%2C%2020kHz%20cfg%201%2C%20fs_5e%2B04.png)

![FFT](./Results/FFT%20result%2C%2020kHz%20cfg%202%2C%20fs_5e%2B04.png)

### Experiments with windowing

![Windows](./Results/Visualized%20data%20from%20from%20CSV%2C%201kHz%20measred%20in%20class%2C%20fs_5e%2B04%2B%20winodows.png)

Using windows also requires to remove DC offset from measurements. I didnt capture a plot wiht it so i can olny show how bad windows behaved for low frequency.

![windowing fft](./Results/FFT%20result%2C%201kHz%20measred%20in%20class%2C%20fs_5e%2B04%2Bwindows.png)

### SINAD and ENOB calcuations

| **Measured frequency** 	| **Configuration** 	| **SINAD [dB]** 	| **ENOB** 	|
|:---:	|:---:	|:---:	|:---:	|
| 1kHz 	| BLOCKING ADC MEASUREMENT DURING CLASS 	| - 	| - 	|
| 1kHz 	| BLOCKING ADC MEASUREMENT AT HOME 	| 31.23 	| 13.73 	|
| 1kHz 	| DMA + x16 oversampling 	| - 	| - 	|
| 10kHz 	| BLOCKING ADC MEASUREMENT DURING CLASS 	| 28.27 	| 8.80 	|
| 10kHz 	| BLOCKING ADC MEASUREMENT AT HOME 	| 25.7 	| 6 	|
| 10kHz 	| DMA + x16 oversampling AT HOME 	| 26.72 	| 7 	|
| 20kHz 	| BLOCKING ADC MEASUREMENT AT HOME 	| 27.67 	| 8 	|
| 20kHz 	| DMA + x16 oversampling AT HOME 	| 30 	| 11.46 	|
| 40Khz 	| BLOCKING ADC MEASUREMENT DURING CLASS 	| 30.40 	| 12.13 	|

Results are not so consistant but it has two causes:

- SINAD calculation requires precise measurement of signal power contained in peak around measured frequency,

- Measurements that I conducted at home used not so stable, floating function generator. This resulted in wider picks at FFT plot.

## Questions

### What is the Nyquist frequency

It is the minimal frequency that we must sample some signal with in order to avoid spectrum leak and aliasing issues. Minimal value is 2 times bigger than measured signal frequency. If we dont respect this sampling rule (sampling lower than 2 * measured frequency) we will get so called aliasing.

It will result in distortion of measured singal and also higher harmonics may appread in the base frequency.

Because of those issues with sampling frequency we are using antyaliasing filters in oreder to attenuate frequencies higher than 1/2 of sampling frequency. Those ale lowpas filters.

### What theoretical ADC resolution can be achieved with x16 oversampling

Oversampling ratio = 2^(2n), where n is number of extra bits

Using x16 oversampling we can get 2 extra bits. Now important thing is to understand what it means. It means that we will get result that will be 2 bites bigger, (effective number of bits will also be 2 bits higher), but we never will be able to have ADC with ENOB = ADC resolution.

Depending on our a

### What are the advantages of using DMA

- Data acquisition ant transfer handled "in the background"
- uC core does not have to waste clock cycles on transfer operations
- event handling - there are a lot of different interrupts informing about transfer complete or half-complete
- DMA can trigger many peripherals on its own
- we can enable DMA and forget about untin all data will be transmitted.

## Author

[**Piotr Zieliński**](https://gitlab.com/piter_t_ziel)
